<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tittle_Shopping Cart</name>
   <tag></tag>
   <elementGuidId>178e1d88-5721-47e1-8b9d-f388a9c5aa37</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.cart-head > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1c335452-f15a-4a0b-91d7-8ddd03a695d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.1.0.0.0</value>
      <webElementGuid>86cdc062-38cd-4ab7-b50b-16a13513b366</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shopping Cart  </value>
      <webElementGuid>09dd1e22-7278-4650-82ae-14569223c63c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;cart-content buying&quot;]/div[@class=&quot;cart-inner&quot;]/div[@class=&quot;cart-head&quot;]/span[1]</value>
      <webElementGuid>989ed3f3-16f3-4a0d-934b-26d8c38b8e1f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div/div[2]/div/div/span</value>
      <webElementGuid>1b783f73-745e-4eaa-a8d4-dc3981b32231</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP to MIDTRANS →'])[1]/following::span[1]</value>
      <webElementGuid>cf7b5305-13fc-411f-9c49-a3c0df06b283</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP  →'])[1]/following::span[1]</value>
      <webElementGuid>e98a512d-7ba1-47a4-b118-076da19cad58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product'])[1]/preceding::span[2]</value>
      <webElementGuid>8b0ac9de-97fc-4b84-9482-9d68218d5da1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty'])[1]/preceding::span[2]</value>
      <webElementGuid>1ead21e6-964a-4e40-b8ff-d108d32b9d53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/span</value>
      <webElementGuid>c65b53e5-d0ee-4c9e-af6a-1146ef01fbd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Shopping Cart  ' or . = 'Shopping Cart  ')]</value>
      <webElementGuid>0b764695-6416-4689-8352-d7d9a2407bcd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
