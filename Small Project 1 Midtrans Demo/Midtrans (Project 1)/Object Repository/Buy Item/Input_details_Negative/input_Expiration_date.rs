<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Expiration_date</name>
   <tag></tag>
   <elementGuidId>cf274afe-ed8c-40b6-b625-79edb09d2434</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#card-expiry</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='card-expiry']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b4c087da-6544-49d7-a6d4-18bb35575329</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>card-expiry</value>
      <webElementGuid>401848d5-5857-46ef-aaae-c6aac96825d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>5ed20ed8-ebfc-4661-b24f-2bf239ab5a33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>MM/YY</value>
      <webElementGuid>1548744c-3729-4912-9b62-612ca5b1415a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-input-value</value>
      <webElementGuid>f68643cd-3eea-4102-9052-0bd5415b0fad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>34458b39-f8d8-4685-a2b0-eb8a4e4a05da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-expiry&quot;)</value>
      <webElementGuid>f16c7a7f-bb67-4c9f-9c46-8461d65377f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Input_details_Negative/iframe_concat(id(, , snap-midtrans, , ))_po_1b7efd</value>
      <webElementGuid>f578ee9c-0c4c-4269-8441-43ea9cce7db5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='card-expiry']</value>
      <webElementGuid>3eedd40d-1e76-4fa8-aade-35c2f9d8b734</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[3]/input</value>
      <webElementGuid>80aef981-665d-4a01-8ab6-d036fa851bb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>a7b9f814-2156-47b6-8fe6-5757f46a2e05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'card-expiry' and @type = 'tel' and @placeholder = 'MM/YY']</value>
      <webElementGuid>5074b9f3-186e-489e-84a1-63054742f4f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
