<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>message_Card got declined by the bank. Try usin_28c355</name>
   <tag></tag>
   <elementGuidId>9e5eeb69-69dd-49fd-8004-b85d6b27a45a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-warning.text-failed</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[2]/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>203aaf30-3345-442b-8d1b-c3676a76ce43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-warning text-failed</value>
      <webElementGuid>b628b10a-8658-4ac6-9c4b-1066390177ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Card got declined by the bank. Try using another card/payment method.</value>
      <webElementGuid>a93b9834-9c32-4e64-9f6d-31834f66583d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;card-number&quot;]/div[@class=&quot;card-warning text-failed&quot;]</value>
      <webElementGuid>66647ad5-11ed-49ef-937f-37a2ab145a62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Input_details_Negative/iframe_concat(id(, , snap-midtrans, , ))_po_1b7efd</value>
      <webElementGuid>708a0508-5859-4bd8-bcba-7ccecba60182</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]/div[3]</value>
      <webElementGuid>704c8df9-bc84-4203-b920-c1b7598cf70e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card number'])[1]/following::div[3]</value>
      <webElementGuid>3d9f1b7e-c74c-4dcd-8275-911cad500d3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit/debit card'])[1]/following::div[5]</value>
      <webElementGuid>bddffcaa-aa5e-43f2-bac3-0f44b1a82e76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/preceding::div[1]</value>
      <webElementGuid>64976640-06a7-4fcd-b8ad-457f3755ffe4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CVV'])[1]/preceding::div[2]</value>
      <webElementGuid>e8ee3f1c-7ed4-44e0-9930-c9fb9e377588</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Card got declined by the bank. Try using another card/payment method.']/parent::*</value>
      <webElementGuid>3854dfc9-1a7b-4d9d-a4de-e90a178e11ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]</value>
      <webElementGuid>944edb64-1f5b-455c-bb11-80c9dc7481ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Card got declined by the bank. Try using another card/payment method.' or . = 'Card got declined by the bank. Try using another card/payment method.')]</value>
      <webElementGuid>1a5d8966-0ae6-4f47-86f6-e078693d4ab4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
