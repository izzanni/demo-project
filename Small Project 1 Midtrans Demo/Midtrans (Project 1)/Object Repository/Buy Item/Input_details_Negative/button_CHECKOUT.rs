<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_CHECKOUT</name>
   <tag></tag>
   <elementGuidId>a13e5041-7f3f-421e-8d45-1d9bc36cfd0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.cart-checkout</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>20b476c4-f171-4585-a73d-915006f2f42d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cart-checkout</value>
      <webElementGuid>ac7ad22a-85d2-474f-9e8a-bae13afb18b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.1.1.0</value>
      <webElementGuid>76afb83e-93d4-4338-b10a-e2cbc58827ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CHECKOUT</value>
      <webElementGuid>00363a8b-fbb6-458c-9c9c-66fce77edcaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;cart-content buying&quot;]/div[@class=&quot;cart-action&quot;]/div[@class=&quot;cart-checkout&quot;]</value>
      <webElementGuid>0c17e353-1be4-422a-b9c2-763c13bdf4cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div/div[2]/div[2]/div</value>
      <webElementGuid>c791a61e-5fbe-433d-bca6-9458e04bf52c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Postal Code'])[1]/following::div[2]</value>
      <webElementGuid>f7f9cee6-4cbe-46dc-8ae7-ed5123f3ac5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MidPlaza 2, 4th Floor Jl.Jend.Sudirman Kav.10-11'])[1]/following::div[2]</value>
      <webElementGuid>cfb5c557-7fa4-45ab-be73-6d03a504d82f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='cancel'])[1]/preceding::div[2]</value>
      <webElementGuid>38fb0c07-f013-49b0-88dd-5356647c3342</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SNAP Features Settings'])[1]/preceding::div[3]</value>
      <webElementGuid>83b86d65-669b-45db-81f7-9c82bd7bfdb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CHECKOUT']/parent::*</value>
      <webElementGuid>635216de-0f47-4034-9272-e1724e64c29d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div</value>
      <webElementGuid>d6675ebb-eb10-434d-80fb-c7d793884ef2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'CHECKOUT' or . = 'CHECKOUT')]</value>
      <webElementGuid>4024ea95-7753-42f4-96ab-654712223599</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
