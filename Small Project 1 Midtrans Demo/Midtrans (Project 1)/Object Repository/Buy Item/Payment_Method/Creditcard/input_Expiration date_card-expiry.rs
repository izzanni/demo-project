<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Expiration date_card-expiry</name>
   <tag></tag>
   <elementGuidId>b4dc2715-5017-4bb4-b374-c8c798343dbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#card-expiry</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='card-expiry']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>30de80cf-c616-49aa-8426-ab92763b67ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>card-expiry</value>
      <webElementGuid>09f2f03a-88ee-4ce4-8564-46ff0caac79c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>9e255be5-0d0f-4440-abb7-ef664e9cd302</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>MM/YY</value>
      <webElementGuid>5b4bb093-29b7-4a64-a2e1-e6ca4197c558</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-input-value</value>
      <webElementGuid>ea935895-0f91-4687-9cbd-9af258086858</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>0c1a2a08-f1a9-4a47-9e22-2de8c1513b79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-expiry&quot;)</value>
      <webElementGuid>e3ecf1e5-f364-4a33-a0bb-211c4bd731e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/Creditcard/iframe_concat(id(, , snap-midtrans, , ))_po_ecfa45</value>
      <webElementGuid>b9ce6d15-0c64-4f09-a12b-ee6c5a2d0389</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='card-expiry']</value>
      <webElementGuid>a8b0d329-e6e7-49c5-bcab-65943ddb245e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[3]/input</value>
      <webElementGuid>468f639f-1cf6-425c-88aa-7fba946c88d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>04c89942-dc37-41a9-842c-31a9b0bd4b0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'card-expiry' and @type = 'tel' and @placeholder = 'MM/YY']</value>
      <webElementGuid>df9bbee7-a2fc-4006-8e12-7083073447ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
