<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Payment successful</name>
   <tag></tag>
   <elementGuidId>473787c6-af5a-43fd-a89e-6412b2a557c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-headline.medium</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c0e5250c-b66c-4e86-806e-1c2eee765275</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-headline medium</value>
      <webElementGuid>0bb3e237-24c2-451a-8e18-f660b0e78a8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment successful</value>
      <webElementGuid>b165db30-781f-46eb-9c86-475debcd4b04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;fullscreen-container&quot;]/div[@class=&quot;success-wrapper&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;text-headline medium&quot;]</value>
      <webElementGuid>76db3487-a7d8-473b-8c78-6207f2469e31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/Creditcard/iframe_concat(id(, , snap-midtrans, , ))_po_ecfa45</value>
      <webElementGuid>389b0e41-9a7a-40cb-bbae-81f1340b85bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div</value>
      <webElementGuid>20c635e2-83e4-4d58-aeaa-2c942a1c1655</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[6]</value>
      <webElementGuid>7ca3aa05-17ae-4a24-b870-046a2231d73e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp15.000'])[1]/preceding::div[1]</value>
      <webElementGuid>40559fbf-4a04-4f51-983d-66c659066a14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order ID #sample-store-1652263165'])[1]/preceding::div[2]</value>
      <webElementGuid>0761c464-ec32-45ff-b9ca-7dec12db3984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Payment successful']/parent::*</value>
      <webElementGuid>5fb9d4a6-506b-48e3-9950-1fbdad0413c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div</value>
      <webElementGuid>1bbca942-4d46-4b27-b743-aa617abcfe85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Payment successful' or . = 'Payment successful')]</value>
      <webElementGuid>550923df-316e-4706-8eb2-ac30e0a9e4b0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
