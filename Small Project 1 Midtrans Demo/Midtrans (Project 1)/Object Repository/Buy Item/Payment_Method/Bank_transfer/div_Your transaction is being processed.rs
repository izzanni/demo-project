<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Your transaction is being processed</name>
   <tag></tag>
   <elementGuidId>9b1fe924-9d4d-4f38-b55c-e494c3334556</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-headline.medium</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2bb3ba43-daee-4e1f-8fea-5237b43536e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-headline medium</value>
      <webElementGuid>4859de60-2e84-4f72-b3af-60d1cf76ca43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your transaction is being processed</value>
      <webElementGuid>9ecf484a-d6fc-4803-b862-0a90d154fa52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;fullscreen-container&quot;]/div[@class=&quot;processed-wrapper&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;text-headline medium&quot;]</value>
      <webElementGuid>50c96591-d2d7-4822-9833-9e4299702659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/Bank_transfer/iframe_concat(id(, , snap-midtrans, , ))_po_bc53fd</value>
      <webElementGuid>675291ae-7a3e-405f-a327-0304334651ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div</value>
      <webElementGuid>c9065a6d-1b79-447d-b765-ea86af48a941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[6]</value>
      <webElementGuid>e67146ec-3bbe-4437-aca9-893165fd7e74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp20.000'])[1]/preceding::div[1]</value>
      <webElementGuid>7058e9f0-e4e4-4861-8f9c-47d1e0ee0d88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you make a payment using an e-wallet or a credit/debit card, you have completed your payment.'])[1]/preceding::div[2]</value>
      <webElementGuid>72d93ee3-29e4-417f-90b2-fc2b5ff37714</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your transaction is being processed']/parent::*</value>
      <webElementGuid>af7c5afb-a731-4512-9f72-950fc450a2a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div</value>
      <webElementGuid>e6e8688d-75ba-4c42-b83c-84d2b6ef95ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Your transaction is being processed' or . = 'Your transaction is being processed')]</value>
      <webElementGuid>c312ce7c-30b2-4ad1-b567-67e5bc8fc568</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
