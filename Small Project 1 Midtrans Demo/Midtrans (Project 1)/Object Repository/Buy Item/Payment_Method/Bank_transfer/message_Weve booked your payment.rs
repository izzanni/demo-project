<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>message_Weve booked your payment</name>
   <tag></tag>
   <elementGuidId>57f98cf2-57d8-4aae-ba6c-310951d9abdd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.trans-status.trans-pending > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div/div/div[2]/div/div[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8cbe404d-0936-4457-9291-df30e1755f53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.0.2.0.1.0.0:0</value>
      <webElementGuid>6414a67f-085f-4d25-a4af-bec4277b5e55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>We've booked your payment.</value>
      <webElementGuid>ba80ca15-99ec-4068-82f1-fe02e9066ac5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;main-content&quot;]/div[@class=&quot;ss-box-wrapper&quot;]/div[@class=&quot;ss-box&quot;]/div[@class=&quot;notification-wrapper&quot;]/div[@class=&quot;trans-status trans-pending&quot;]/span[1]</value>
      <webElementGuid>86aa7425-6882-4354-818d-8d102cf55f42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div/div/div[2]/div/div[2]/div/span</value>
      <webElementGuid>1bd86ed7-df99-4ff2-afe5-c7bc06e46f01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP  →'])[1]/following::span[1]</value>
      <webElementGuid>2bf8dfed-4619-47c5-91d7-7ad82838f417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ready to integrate SNAP?'])[1]/following::span[1]</value>
      <webElementGuid>0150ce76-cecf-4bac-8576-057a32cda95f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please continue as instructed.'])[1]/preceding::span[1]</value>
      <webElementGuid>ec7d63ca-9e4d-4b32-83c8-77aeda80dc92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP to MIDTRANS →'])[1]/preceding::span[2]</value>
      <webElementGuid>903a1f84-1071-4b9f-9c93-32206df45404</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/span</value>
      <webElementGuid>ec83c52f-ecb7-4aba-87f7-7a1063642a41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve booked your payment.&quot;) or . = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve booked your payment.&quot;))]</value>
      <webElementGuid>48593c89-f6ef-440c-9001-778fbaf1f5a9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
