<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_method_bank_transfer</name>
   <tag></tag>
   <elementGuidId>aa01e405-3ecb-49f4-aed6-9a4f122a939a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/a[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>49ab7b4b-216d-4a04-8c48-499206285397</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-payment-logo</value>
      <webElementGuid>2129eeca-1043-4df7-9ad2-9408cb810d76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/a[@class=&quot;list&quot;]/div[@class=&quot;list-content&quot;]/div[@class=&quot;list-payment-logo&quot;]</value>
      <webElementGuid>3636531a-680d-42b1-afc1-b6bda8be9b63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/Bank_transfer/iframe_concat(id(, , snap-midtrans, , ))_po_bc53fd</value>
      <webElementGuid>a652e746-ebc7-4654-a8c4-94ee0e78666f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/a[2]/div/div[2]</value>
      <webElementGuid>93e6408a-dab8-43d5-b99f-7c05cbd96424</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::div[1]</value>
      <webElementGuid>35325bf0-448d-4de6-a194-ee1cb949ecbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Promo available'])[1]/following::div[10]</value>
      <webElementGuid>ef4d0843-4f2e-4dbd-9a64-2ed2b6d984ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GoPay/other e-Wallets'])[1]/preceding::div[9]</value>
      <webElementGuid>18706d91-dbc9-42ed-89cd-16fd35987bc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ShopeePay/other e-Wallets'])[1]/preceding::div[16]</value>
      <webElementGuid>64c7afd7-2c0c-4e62-a46d-6e74053307e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/div/div[2]</value>
      <webElementGuid>4b5ae041-1f78-4866-874e-f1a74fbac3ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
