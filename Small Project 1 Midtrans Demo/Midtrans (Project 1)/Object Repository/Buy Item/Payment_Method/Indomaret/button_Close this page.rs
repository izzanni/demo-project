<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Close this page</name>
   <tag></tag>
   <elementGuidId>6b3b31bb-22be-4437-b116-5252e29f02e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.full.primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>17337fcb-a4ba-4850-b505-c61605860e8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0a4abe14-c18b-4930-942b-02b49777b6df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>682ca53e-93dc-47e1-a022-7ed0ec39f7fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close this page</value>
      <webElementGuid>8cfd33bd-fa7b-4a99-b8e7-a10e5f13549f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;cstore-button&quot;]/button[@class=&quot;btn full primary&quot;]</value>
      <webElementGuid>ec018267-3aa4-4dde-904e-89805adb959f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/Indomaret/iframe_concat(id(, , snap-midtrans, , ))_po_308cce</value>
      <webElementGuid>b566daaf-a917-4266-adf4-bbed185be76d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[2]</value>
      <webElementGuid>af526c97-a836-4153-83fd-1e9395b10831</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[7]/button</value>
      <webElementGuid>9ac9cc42-19fd-4a28-9498-bb0de8194b9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download payment info'])[1]/following::button[1]</value>
      <webElementGuid>4c587ab6-beda-4761-a5cf-626948b6e0a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please keep your Indomaret payment receipt in case you’ll need further help via support.'])[1]/following::button[2]</value>
      <webElementGuid>17a3aa45-d3ba-48d9-b912-586d0ef74e2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Close this page']/parent::*</value>
      <webElementGuid>fdb7e2f2-3f1a-4929-9656-5fa908df21ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/button</value>
      <webElementGuid>d44d7eff-40fa-4f1c-a3c4-8fd86d9ac8d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Close this page' or . = 'Close this page')]</value>
      <webElementGuid>b3c9d857-ad0d-470d-817e-b013ac71e7e9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
