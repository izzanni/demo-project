<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Your transaction is being processed</name>
   <tag></tag>
   <elementGuidId>4a31ce07-489c-43b4-b399-2087b5d75116</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-headline.medium</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>91617007-d03b-4914-a016-8dcf4c5f34fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-headline medium</value>
      <webElementGuid>db146e5f-0e48-4488-b834-e6c2e32bf96d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your transaction is being processed</value>
      <webElementGuid>66b9d186-0251-41ed-ae3b-16be645af00b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;fullscreen-container&quot;]/div[@class=&quot;processed-wrapper&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;text-headline medium&quot;]</value>
      <webElementGuid>bf1178e0-0ff2-4fee-9595-f42c216fc689</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/GoPay/iframe_concat(id(, , snap-midtrans, , ))_po_21e61b</value>
      <webElementGuid>0ab6080f-194d-4352-abae-8b1f414df9c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div</value>
      <webElementGuid>258664dc-bc5b-448b-a0ab-0e3e5dc4bf9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[6]</value>
      <webElementGuid>f6e9977c-f0c5-492b-a415-98f9b5f7d009</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp20.000'])[1]/preceding::div[1]</value>
      <webElementGuid>30f2cc28-139c-4667-b60e-496984ede4b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you make a payment using an e-wallet or a credit/debit card, you have completed your payment.'])[1]/preceding::div[2]</value>
      <webElementGuid>bf066af4-2869-401b-9fce-6b6a1493d79a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your transaction is being processed']/parent::*</value>
      <webElementGuid>4ed6227a-94fd-40fd-859f-56854610775f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div</value>
      <webElementGuid>efcac026-c4ad-462d-9eff-4f683e797094</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Your transaction is being processed' or . = 'Your transaction is being processed')]</value>
      <webElementGuid>ed0edd89-b33b-475c-8fbc-b38e4c1d0f2c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
