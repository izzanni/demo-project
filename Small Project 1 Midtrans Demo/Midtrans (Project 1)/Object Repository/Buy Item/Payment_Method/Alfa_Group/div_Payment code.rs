<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Payment code</name>
   <tag></tag>
   <elementGuidId>e60301c5-b742-4a8b-a0a5-651f69b24bf0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.payment-page-layout.payment-page-text > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f321a1c2-bb14-4a3e-a602-45689b24f5ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Payment code</value>
      <webElementGuid>2f00ad3e-53a9-4a9a-86c3-4a654ca04e4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-page-layout payment-page-text&quot;]/div[1]</value>
      <webElementGuid>2620b226-125b-4537-8ae2-a58a9a5a7861</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/Alfa_Group/iframe_concat(id(, , snap-midtrans, , ))_po_5298b6</value>
      <webElementGuid>cfff9285-9f1c-46f0-af94-275eb0de73fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[3]/div</value>
      <webElementGuid>cc6bcdef-ad39-47de-9f76-e6f6917cfaa1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please go to nearest Alfa Group store and show the barcode/payment code to the cashier.'])[1]/following::div[2]</value>
      <webElementGuid>3289801b-573b-4177-b7a8-4d0d03a9fb3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alfa Group'])[1]/following::div[3]</value>
      <webElementGuid>49df1e4b-ec56-4a01-94d8-60cbab3eb0f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='How to pay'])[1]/preceding::div[4]</value>
      <webElementGuid>7026387c-c1a7-4240-8151-f2ba4beacb0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tap Download payment info to get a copy of your unique payment details.'])[1]/preceding::div[6]</value>
      <webElementGuid>af18ef79-734a-4927-9990-68e92932327e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Payment code']/parent::*</value>
      <webElementGuid>201737ac-08f3-4de2-af86-04bcaa00b127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div</value>
      <webElementGuid>3c3a9b81-56fe-48e5-a9c5-382caf1c29a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Payment code' or . = 'Payment code')]</value>
      <webElementGuid>0df40b94-22ec-4a0c-87b1-b782d308eaf6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
