<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Weve booked your payment</name>
   <tag></tag>
   <elementGuidId>af4e873e-def3-4234-9e32-225231414d62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.trans-status.trans-pending > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div/div/div[2]/div/div[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ec099968-a58f-4f1c-8715-a86f6ffde637</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.0.2.0.1.0.0:0</value>
      <webElementGuid>361925ae-d141-4372-915b-dafaa69c697f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>We've booked your payment.</value>
      <webElementGuid>3cbea15a-34e6-4d72-b5b6-285ab88c00e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;main-content&quot;]/div[@class=&quot;ss-box-wrapper&quot;]/div[@class=&quot;ss-box&quot;]/div[@class=&quot;notification-wrapper&quot;]/div[@class=&quot;trans-status trans-pending&quot;]/span[1]</value>
      <webElementGuid>f1a85283-4c38-40ef-aa89-5ac7b9e2b084</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div/div/div[2]/div/div[2]/div/span</value>
      <webElementGuid>2f8f83ea-f60c-4e51-9417-a2d90ce5ec30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP  →'])[1]/following::span[1]</value>
      <webElementGuid>e93e8354-57a1-407b-af52-42f5ef0f53c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ready to integrate SNAP?'])[1]/following::span[1]</value>
      <webElementGuid>2febd6d3-95a6-438c-b90c-07dbad88af5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please continue as instructed.'])[1]/preceding::span[1]</value>
      <webElementGuid>143a063f-2376-44c8-8b71-cc3f8bf053d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP to MIDTRANS →'])[1]/preceding::span[2]</value>
      <webElementGuid>5810a73a-47a4-4994-ae0c-4d8fc59d6c1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/span</value>
      <webElementGuid>baf9c5fc-ac3a-4ed9-a06a-30d9831e1ed7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve booked your payment.&quot;) or . = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve booked your payment.&quot;))]</value>
      <webElementGuid>60acc04b-ec1f-4a6c-b42e-e054193367d8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
