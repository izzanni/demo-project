<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Transaction is Successful</name>
   <tag></tag>
   <elementGuidId>db2b2297-f062-41a9-a899-2d48207a9023</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Akulaku'])[2]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>13a5dd1b-6ddc-4eba-bcc5-5f838ace32ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success</value>
      <webElementGuid>5c0658f0-5fc6-4538-92a0-55d0893499e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Transaction is Successful</value>
      <webElementGuid>59c202dc-68c2-455c-817c-5dba9dbe0788</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]/div[@class=&quot;alert alert-success&quot;]</value>
      <webElementGuid>65640da7-fcc4-43c2-98f2-f2f33a8fc915</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akulaku'])[2]/following::div[1]</value>
      <webElementGuid>897bb6d7-f31d-4648-9566-327c518b344c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret Endpoint'])[1]/following::div[3]</value>
      <webElementGuid>d00bbf0d-8b83-4072-a123-b4464eb68f58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to merchant website'])[1]/preceding::div[1]</value>
      <webElementGuid>59601a55-ca65-44bb-830d-f6be93de3961</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Transaction is Successful']/parent::*</value>
      <webElementGuid>e087addb-a3c6-4b46-96fe-7145b13fa8f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]</value>
      <webElementGuid>6145d0e4-ffe9-47c3-afd7-928a90d0ab82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Transaction is Successful' or . = 'Transaction is Successful')]</value>
      <webElementGuid>9e32eb4c-f101-4645-8085-d87a215d9cca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
