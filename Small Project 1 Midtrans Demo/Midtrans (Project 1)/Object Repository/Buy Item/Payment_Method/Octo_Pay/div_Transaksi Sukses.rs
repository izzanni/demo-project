<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Transaksi Sukses</name>
   <tag></tag>
   <elementGuidId>a8ce7b80-aa92-4507-a13c-0ea6275f3641</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Cimb Clicks'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>38908874-4edc-4fc3-8ead-dda52cd71262</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success</value>
      <webElementGuid>d1543f94-d2ce-495d-9107-0bee19141607</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Transaksi Sukses</value>
      <webElementGuid>31152368-2023-4ef2-a47b-5d5ce79d96ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;container&quot;]/div[@class=&quot;alert alert-success&quot;]</value>
      <webElementGuid>94a797ca-b1ab-4be1-b5c0-c75ae0e167a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cimb Clicks'])[1]/following::div[1]</value>
      <webElementGuid>ffd8c198-170f-4e2d-bb44-f2d8910ed48d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret Endpoint'])[1]/following::div[3]</value>
      <webElementGuid>12144370-dac5-4cce-8422-eda507d0ce2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kembali ke website Merchant'])[1]/preceding::div[1]</value>
      <webElementGuid>168287a6-f085-45c3-b45d-eee42d228289</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Transaksi Sukses']/parent::*</value>
      <webElementGuid>954e9a10-4c64-4f74-b6c8-a9d74744fea9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]</value>
      <webElementGuid>70c00a02-cadd-4798-96a2-3277a055ac1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Transaksi Sukses' or . = 'Transaksi Sukses')]</value>
      <webElementGuid>c946dae0-c117-4993-ad65-b3939927853b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
