<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Your transaction is being processed</name>
   <tag></tag>
   <elementGuidId>e04f115c-fb2b-4d57-a822-95349bbcb25c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-headline.medium</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ca0bf0bf-25eb-43ac-a866-e2871e0f370b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-headline medium</value>
      <webElementGuid>9975250f-ee75-43af-ab63-47016ab9c3bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Your transaction is being processed</value>
      <webElementGuid>f001db0d-b66c-4883-ae2b-ecf27aa2d2cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;fullscreen-container&quot;]/div[@class=&quot;processed-wrapper&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;text-headline medium&quot;]</value>
      <webElementGuid>393b7e12-6fec-42e5-99e5-6107455c9c6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/ShopeePay/iframe_concat(id(, , snap-midtrans, , ))_po_1969bb</value>
      <webElementGuid>3b353843-4500-4f88-9d3b-c5952303c6ef</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div</value>
      <webElementGuid>9f79b86f-5850-46d2-a0f0-d671701d5a75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[6]</value>
      <webElementGuid>6c2181a1-cf6b-4412-a9da-8978d3f9fa85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp20.000'])[1]/preceding::div[1]</value>
      <webElementGuid>cce3f8f7-cdf1-4bd7-aa08-6fb785bf20b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you make a payment using an e-wallet or a credit/debit card, you have completed your payment.'])[1]/preceding::div[2]</value>
      <webElementGuid>99df9bbb-b33c-46d7-ac9d-de270aa27945</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your transaction is being processed']/parent::*</value>
      <webElementGuid>766ea2df-56d3-4a00-9eb0-a6f06dfb1a66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div</value>
      <webElementGuid>b329e5c0-231f-4bde-ad4d-c39375cad6c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Your transaction is being processed' or . = 'Your transaction is being processed')]</value>
      <webElementGuid>409c0a8f-292f-4892-a98a-a973b8cb33be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
