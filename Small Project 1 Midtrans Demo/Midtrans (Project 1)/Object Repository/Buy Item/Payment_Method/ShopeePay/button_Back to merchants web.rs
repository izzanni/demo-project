<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Back to merchants web</name>
   <tag></tag>
   <elementGuidId>cb269b93-bf0a-4927-92d9-92cf93ce4d8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.full.primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>81e0bd72-019c-4e62-851a-3b4e57cf0468</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c52cb93a-92d3-4b6b-b803-c21d0044dd4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>a94e6a40-ddc4-4a29-a64c-824c7f6be2b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Back to merchant's web</value>
      <webElementGuid>11d3de7a-a9d0-4385-8d36-2f214d773d21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;fullscreen-container&quot;]/div[@class=&quot;bottom&quot;]/button[@class=&quot;btn full primary&quot;]</value>
      <webElementGuid>f5d31082-a2f9-4f91-b4d7-852611545797</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Payment_Method/ShopeePay/iframe_concat(id(, , snap-midtrans, , ))_po_1969bb</value>
      <webElementGuid>50eaee26-8e37-41e7-bb5a-f74608f51046</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>e37e5d58-f71c-41bf-9c08-732b134fb15a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]/button</value>
      <webElementGuid>85a7a849-241a-4a77-9a54-85208a712417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you made a payment via bank transfer, please continue the payment process.'])[1]/following::button[1]</value>
      <webElementGuid>61bedab9-cb67-4ceb-8c34-6ead4fe3775e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you make a payment using an e-wallet or a credit/debit card, you have completed your payment.'])[1]/following::button[1]</value>
      <webElementGuid>b76dfcee-3b18-4412-97b8-c287b40eaa30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>1be19214-d465-4655-96aa-ed8b63e047a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = concat(&quot;Back to merchant&quot; , &quot;'&quot; , &quot;s web&quot;) or . = concat(&quot;Back to merchant&quot; , &quot;'&quot; , &quot;s web&quot;))]</value>
      <webElementGuid>39d4701b-8855-411d-b802-2760db1fbc2d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
