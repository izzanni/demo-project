<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Weve booked your payment</name>
   <tag></tag>
   <elementGuidId>dbe18445-8e1a-4c45-b0d3-d5fac1952a22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.trans-status.trans-pending > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div/div/div[2]/div/div[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c6535457-38ad-4d54-bf75-055ff0b859e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.0.2.0.1.0.0:0</value>
      <webElementGuid>de6568f4-c0f3-4279-90da-ac0ae36041cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>We've booked your payment.</value>
      <webElementGuid>0a697638-6177-44c0-97d2-265abcd77513</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;main-content&quot;]/div[@class=&quot;ss-box-wrapper&quot;]/div[@class=&quot;ss-box&quot;]/div[@class=&quot;notification-wrapper&quot;]/div[@class=&quot;trans-status trans-pending&quot;]/span[1]</value>
      <webElementGuid>738f3750-301e-4fb7-a605-31284b2a379a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div/div/div[2]/div/div[2]/div/span</value>
      <webElementGuid>680d6cb3-310c-42b3-8658-a6d2ee93bd12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP  →'])[1]/following::span[1]</value>
      <webElementGuid>a1470a72-51fd-41d8-9c42-9313b8bc3d5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ready to integrate SNAP?'])[1]/following::span[1]</value>
      <webElementGuid>d3b1d422-98c2-456d-b31c-84aed6cfcd44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please continue as instructed.'])[1]/preceding::span[1]</value>
      <webElementGuid>d317a03e-6c09-433d-b2e5-4495476bf885</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SIGN UP to MIDTRANS →'])[1]/preceding::span[2]</value>
      <webElementGuid>77196222-57e3-4833-b5be-ce255c35ac6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/span</value>
      <webElementGuid>d353bbe6-5d06-487b-bcc3-ee6c76e92e03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve booked your payment.&quot;) or . = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve booked your payment.&quot;))]</value>
      <webElementGuid>70684941-e151-4013-91a1-103cb3e2398c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
