<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_concat(id(, , snap-midtrans, , ))_po_5ccd86</name>
   <tag></tag>
   <elementGuidId>2acb0248-820b-40e3-ba59-073a43da050a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='snap-midtrans']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#snap-midtrans</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>adb96c54-b4a9-495b-b762-bf9510db6e9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo.midtrans.com&amp;digest=6937ec1b596f47bd061fe00c7d22c4dd5ac569dd2cb3cc17792aac5f886eb38c&amp;client_key=VT-client-yrHf-c8Sxr-ck8tx#/</value>
      <webElementGuid>545ade6e-5fdf-4546-a21e-02da9ea45984</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>9fd2bbfd-ef02-4b55-834d-e26a65d301c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>popup_1652168364497</value>
      <webElementGuid>b4cbb82b-d33b-46c8-a9bd-9bfa86cddd36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wfd-invisible</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>893e06a9-9363-4034-bfab-c9d433e84a5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-midtrans&quot;)</value>
      <webElementGuid>54ee25f6-fb19-472e-b2b7-50e7100c83ad</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='snap-midtrans']</value>
      <webElementGuid>8b4a8032-7ed9-481d-a593-54697a6c161c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>9bc59ead-b45a-4135-b327-b71994e91c17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo.midtrans.com&amp;digest=6937ec1b596f47bd061fe00c7d22c4dd5ac569dd2cb3cc17792aac5f886eb38c&amp;client_key=VT-client-yrHf-c8Sxr-ck8tx#/' and @id = 'snap-midtrans' and @name = 'popup_1652168364497']</value>
      <webElementGuid>6baad2c4-4eb4-4c6c-be7b-0369573fb313</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
