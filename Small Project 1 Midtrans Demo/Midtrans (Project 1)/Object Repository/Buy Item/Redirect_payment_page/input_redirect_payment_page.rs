<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_redirect_payment_page</name>
   <tag></tag>
   <elementGuidId>31007f97-c3d6-4ea9-90f7-c153cbe459d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='is_snap_pop_up:false']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1945faa7-bae9-4baf-856b-e993cb6587b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>a0c2f6b7-5c57-4365-9fc6-98b8c9b2822d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>is_snap_pop_up</value>
      <webElementGuid>0f14ad14-3629-44ed-8e0e-1806478352f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>is_snap_pop_up:false</value>
      <webElementGuid>97919e6f-4ee7-489c-8d0c-77ae22b761eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0e0e1648-0cfa-4372-9e8e-f8d8072fa931</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.1.2.1.0.5.1.1.3.0</value>
      <webElementGuid>20720d3f-d8bc-4f12-ac9e-95e91acb0b88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;is_snap_pop_up:false&quot;)</value>
      <webElementGuid>8a84cd1c-7517-49c7-b616-d53f2b22b7e6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='is_snap_pop_up:false']</value>
      <webElementGuid>51908ec0-5a6f-451d-b9c5-42490102b9a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main_setting_div_custom']/div/div/div[4]/input</value>
      <webElementGuid>a96f5f93-26dc-414b-971e-0574cef69780</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[4]/input</value>
      <webElementGuid>eaf2cff4-b9b1-46fe-a21c-dd11cb745fb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'is_snap_pop_up' and @id = 'is_snap_pop_up:false']</value>
      <webElementGuid>db0c4778-2d82-423b-aaa3-b89cfde94a84</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
