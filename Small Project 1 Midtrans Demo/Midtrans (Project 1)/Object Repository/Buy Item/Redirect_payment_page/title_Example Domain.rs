<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>title_Example Domain</name>
   <tag></tag>
   <elementGuidId>8d891d39-bf0a-4f65-a587-5c01e67fdce2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='More information...'])[1]/preceding::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>4baf6ae4-1008-4a55-bbe3-8b54baf7d7c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Example Domain</value>
      <webElementGuid>b149ee13-34a8-434e-bf4c-5990d6936168</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[1]/h1[1]</value>
      <webElementGuid>b3b78692-4822-4943-87d7-7f17379bc056</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='More information...'])[1]/preceding::h1[1]</value>
      <webElementGuid>8e1ffa4f-1abc-4e59-a075-98356736b9d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/html[1]/body[1]/div[1]/h1[1]'])[1]/preceding::h1[1]</value>
      <webElementGuid>2a342c52-bf02-438c-a983-6a1c720e082c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>f0a50031-ef39-433a-8ffd-abd794f08add</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Example Domain' or . = 'Example Domain')]</value>
      <webElementGuid>cf6bf781-568a-4ba1-b1c8-082604e63c3d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
