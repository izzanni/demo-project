<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_CHECKOUT</name>
   <tag></tag>
   <elementGuidId>17a60257-a4aa-42bf-98e7-e153f61fea08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.cart-checkout</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div/div[2]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>60f4de76-b355-40c4-a725-97278804ae0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cart-checkout</value>
      <webElementGuid>2df9b591-1a25-4d4d-988d-2460b5ed03ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-reactid</name>
      <type>Main</type>
      <value>.0.0.1.1.0</value>
      <webElementGuid>1de02858-17fe-41fb-b42f-f13c5da5011e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CHECKOUT</value>
      <webElementGuid>313d1687-fb56-4c66-bb13-a77ed6647e4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;cart-content buying&quot;]/div[@class=&quot;cart-action&quot;]/div[@class=&quot;cart-checkout&quot;]</value>
      <webElementGuid>e1e866df-d50d-4748-8e8f-a41560350bf0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div/div[2]/div[2]/div</value>
      <webElementGuid>de782931-f4c7-4c67-bf4c-1b6f17178ea5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Postal Code'])[1]/following::div[2]</value>
      <webElementGuid>f995d6a4-268c-4fb6-ab68-9552cd0af4c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MidPlaza 2, 4th Floor Jl.Jend.Sudirman Kav.10-11'])[1]/following::div[2]</value>
      <webElementGuid>ef4c1bd1-10d6-43fa-9463-033c18c5c737</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='cancel'])[1]/preceding::div[2]</value>
      <webElementGuid>148d4200-bfe0-4d43-ba01-1399c9862d9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SNAP Features Settings'])[1]/preceding::div[3]</value>
      <webElementGuid>90117d8d-cd80-423f-8d2e-b44e9e751d0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CHECKOUT']/parent::*</value>
      <webElementGuid>dd0358e0-2e45-4462-ac2c-db4364fa7a11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div</value>
      <webElementGuid>bc4ca5ca-aef6-4728-9bb4-d4900416ce4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'CHECKOUT' or . = 'CHECKOUT')]</value>
      <webElementGuid>6fa160fa-fac4-4288-83f8-8b84fa0af77d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
