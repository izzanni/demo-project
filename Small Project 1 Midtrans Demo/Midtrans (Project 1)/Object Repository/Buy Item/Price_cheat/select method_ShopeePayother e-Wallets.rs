<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select method_ShopeePayother e-Wallets</name>
   <tag></tag>
   <elementGuidId>342bd16c-4fbd-4672-804c-de9162eba70c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/a[4]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9fb02452-0af2-4837-afba-b6947da83164</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-title text-actionable-bold</value>
      <webElementGuid>757d5dcb-22d5-4ea4-88f6-f5c6325a65b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ShopeePay/other e-Wallets</value>
      <webElementGuid>129aea37-2990-458a-941a-1c200d0eb11f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/a[@class=&quot;list&quot;]/div[@class=&quot;list-content&quot;]/div[@class=&quot;list-title text-actionable-bold&quot;]</value>
      <webElementGuid>0b103c75-928a-4766-82c2-29691a76ee1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Price_cheat/iframe_Save settings_popup_1652232693973</value>
      <webElementGuid>0ecb3288-8f83-492d-bad5-f81868dad26f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/a[4]/div/div</value>
      <webElementGuid>2ba572bd-8aa2-4541-bc4b-11fe0f98a015</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GoPay/other e-Wallets'])[1]/following::div[7]</value>
      <webElementGuid>844432d9-a947-41a2-b1a5-5d2a163a8581</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::div[18]</value>
      <webElementGuid>53b1409c-58c0-4ed2-bdb1-3420569d4c7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OCTO Clicks'])[1]/preceding::div[6]</value>
      <webElementGuid>5bd2b103-11f7-42c9-800d-78ce585a7d06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret'])[1]/preceding::div[12]</value>
      <webElementGuid>6af01053-eeff-4202-8c46-4aedcb3a75db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ShopeePay/other e-Wallets']/parent::*</value>
      <webElementGuid>43f3b618-f6a0-4a78-9d96-f68c0f449a92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[4]/div/div</value>
      <webElementGuid>dd6db024-de6e-4534-afc5-c5b62d80583a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'ShopeePay/other e-Wallets' or . = 'ShopeePay/other e-Wallets')]</value>
      <webElementGuid>09207838-8324-4637-bc6b-c843dc557d5c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
