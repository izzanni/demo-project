<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Back to merchants web</name>
   <tag></tag>
   <elementGuidId>dbb5127a-572e-43ab-8292-1cac788beaf7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.full.primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2a5ba7cb-e52d-4d71-a6d5-8f8f150c2f17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3c2ea9ea-967c-4dab-90ff-2b48c8ec2fd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>915ae57b-1373-4e7e-b3a7-30d8e7794491</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Back to merchant's web</value>
      <webElementGuid>808bbf16-b936-46cc-b1c8-4ba21030595b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;fullscreen-container&quot;]/div[@class=&quot;bottom&quot;]/button[@class=&quot;btn full primary&quot;]</value>
      <webElementGuid>886593d3-53a1-4ab7-8086-b679088b1b31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Price_cheat/iframe_Save settings_popup_1652232693973</value>
      <webElementGuid>02ec8e2e-be18-4f78-bb42-fa9867e5db5b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>298cf307-2dc7-4922-8ef5-f11cf3ccb037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[2]/button</value>
      <webElementGuid>d4baf6c1-b148-49fa-ba03-4de48e72c1cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you made a payment via bank transfer, please continue the payment process.'])[1]/following::button[1]</value>
      <webElementGuid>dfaff8d7-85e4-48a4-8f61-5b52b940535d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you make a payment using an e-wallet or a credit/debit card, you have completed your payment.'])[1]/following::button[1]</value>
      <webElementGuid>f8015f73-f83e-4059-b68d-69afb5386e24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>3eb11e23-20a4-4fac-840a-215f9114e5fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = concat(&quot;Back to merchant&quot; , &quot;'&quot; , &quot;s web&quot;) or . = concat(&quot;Back to merchant&quot; , &quot;'&quot; , &quot;s web&quot;))]</value>
      <webElementGuid>fa1c81b0-65d7-45eb-884b-8388f086b5ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
