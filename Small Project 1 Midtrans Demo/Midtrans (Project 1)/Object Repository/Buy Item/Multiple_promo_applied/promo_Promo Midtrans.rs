<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>promo_Promo Midtrans</name>
   <tag></tag>
   <elementGuidId>db633b21-9604-4647-abed-2640cf2c8793</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[5]/div[4]/div/span/label/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1417e64a-e807-46c7-84fb-d129d2ba5acc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Promo Midtrans</value>
      <webElementGuid>ea37c6a4-de52-46a1-8a31-98bc1543d00c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;promo-block&quot;]/div[@class=&quot;promo-radio-input&quot;]/div[@class=&quot;asphalt-theme-a6420f20&quot;]/span[@class=&quot;radioWrap__2LgxN&quot;]/label[@class=&quot;RadioLabel__bLuEy m__3uz4P&quot;]/span[1]</value>
      <webElementGuid>be657ace-9426-4ffd-a1f6-1a1aa67792c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Multiple_promo_applied/iframe_concat(id(, , snap-midtrans, , ))_po_f6bd36</value>
      <webElementGuid>2f187be3-bf8d-4c61-b4f2-7a3a3663b9f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[5]/div[4]/div/span/label/span</value>
      <webElementGuid>e1af83e3-c05a-4ae8-a9b7-da8f0c00f0bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-Rp2.000'])[1]/following::span[2]</value>
      <webElementGuid>71f6dd1a-bd10-40d0-a7cc-8f9d1dc5541f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Potongan 10% - Demo Promo Engine'])[1]/following::span[3]</value>
      <webElementGuid>dd26733e-7d60-472f-8a82-d55039d097c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-Rp2.000'])[2]/preceding::span[1]</value>
      <webElementGuid>f25da1b0-2587-4f93-a8e7-5c141419b436</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Potongan 10 Rupiah'])[1]/preceding::span[2]</value>
      <webElementGuid>e9dc2e21-fa79-4b64-b9dd-1cc3e67f72d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Promo Midtrans']/parent::*</value>
      <webElementGuid>2ea09aba-e9e1-45c3-aa53-1e6bf9350061</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/span/label/span</value>
      <webElementGuid>c4c1d66a-24be-4917-abbb-6390b6c9d1db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Promo Midtrans' or . = 'Promo Midtrans')]</value>
      <webElementGuid>2cce454e-86e2-440e-a67e-e9819b75e86f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
