<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>promo_Promo Installment</name>
   <tag></tag>
   <elementGuidId>fa680dfb-f73d-4f3c-ac65-fdea9baa2410</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.RadioLabel__bLuEy.m__3uz4P > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[5]/div[2]/div/span/label/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>42014c7c-e4c8-44d5-8e58-172949262dd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Promo Installment</value>
      <webElementGuid>644ceb5a-6103-4f5c-b833-9930cd87c7e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;promo-block&quot;]/div[@class=&quot;promo-radio-input&quot;]/div[@class=&quot;asphalt-theme-a6420f20&quot;]/span[@class=&quot;radioWrap__2LgxN&quot;]/label[@class=&quot;RadioLabel__bLuEy m__3uz4P&quot;]/span[1]</value>
      <webElementGuid>345d275d-2957-40c6-a9d9-74b33f090d6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Multiple_promo_applied/iframe_concat(id(, , snap-midtrans, , ))_po_f6bd36</value>
      <webElementGuid>b87add41-c0e1-42e8-afb8-a66388c14787</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[5]/div[2]/div/span/label/span</value>
      <webElementGuid>5594d30a-1502-4bdf-93ae-18eb52fe836f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select promo'])[1]/following::span[2]</value>
      <webElementGuid>7ca49afd-9ad0-4b90-85c6-0f474a080e59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CVV'])[1]/following::span[2]</value>
      <webElementGuid>625dda10-e73d-4407-91b3-8ae75a95842a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-Rp5.000'])[1]/preceding::span[1]</value>
      <webElementGuid>e83ada44-0e87-4046-a0c2-3799289865ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Potongan 10% - Demo Promo Engine'])[1]/preceding::span[2]</value>
      <webElementGuid>932b76dc-b84a-4862-b36a-15d95b30887e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Promo Installment']/parent::*</value>
      <webElementGuid>d637f138-746d-4c81-a0b6-2d38a278f49f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/span</value>
      <webElementGuid>2ad5baec-724b-49b0-973d-0c340f443722</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Promo Installment' or . = 'Promo Installment')]</value>
      <webElementGuid>6e4e9adf-bb49-42ea-bba3-538ef6ab87b2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
