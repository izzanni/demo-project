<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>promo_Potongan 10 - Demo Promo Engine</name>
   <tag></tag>
   <elementGuidId>70b1c0a6-735c-4615-addc-282a5a90df81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[5]/div[3]/div/span/label/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>68dbd914-96a6-463d-9380-a32156176c01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Potongan 10% - Demo Promo Engine</value>
      <webElementGuid>2f9c2ae8-c800-41d5-98c8-dbb3927f5bc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;promo-block&quot;]/div[@class=&quot;promo-radio-input&quot;]/div[@class=&quot;asphalt-theme-a6420f20&quot;]/span[@class=&quot;radioWrap__2LgxN&quot;]/label[@class=&quot;RadioLabel__bLuEy m__3uz4P&quot;]/span[1]</value>
      <webElementGuid>36b0005f-8a42-4755-9d0d-12109aa9324c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Multiple_promo_applied/iframe_concat(id(, , snap-midtrans, , ))_po_f6bd36</value>
      <webElementGuid>5e1c7977-9537-4ae9-b8a1-672f8498ae9a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[5]/div[3]/div/span/label/span</value>
      <webElementGuid>f51967ba-da37-41e8-96f9-bfa4f38cee4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-Rp5.000'])[1]/following::span[2]</value>
      <webElementGuid>5247f854-9fff-4462-89f5-cba8007d0f31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Promo Installment'])[1]/following::span[3]</value>
      <webElementGuid>8d54c370-5e62-45c4-8de4-c745e3759c57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-Rp2.000'])[1]/preceding::span[1]</value>
      <webElementGuid>dfb8062a-c612-495c-a744-c638e3405ff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Promo Midtrans'])[1]/preceding::span[2]</value>
      <webElementGuid>d0126587-f699-47b4-9122-435727daefb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Potongan 10% - Demo Promo Engine']/parent::*</value>
      <webElementGuid>4f157953-9142-4fe0-8989-a8ff8fe2ca1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/span/label/span</value>
      <webElementGuid>f40914d5-dca6-43b4-bd38-afc48a1c6198</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Potongan 10% - Demo Promo Engine' or . = 'Potongan 10% - Demo Promo Engine')]</value>
      <webElementGuid>9c23b4d7-4298-42ff-a3ac-9f8b48ec613a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
