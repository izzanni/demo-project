<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Rp18.000</name>
   <tag></tag>
   <elementGuidId>24370add-eebd-43bc-a457-e138ea94d2e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.header-amount</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//nav[@id='header']/div[2]/div/div/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>37412a04-f4e2-4e32-b98f-d64db609244c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header-amount</value>
      <webElementGuid>f7722a70-a182-43ce-916d-a1328605fb73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Rp18.000</value>
      <webElementGuid>2b808dcb-3b64-4ee1-8c57-c4aa6efe4c43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header&quot;)/div[@class=&quot;order-header&quot;]/div[@class=&quot;order-box&quot;]/div[@class=&quot;order-summary-section&quot;]/div[@class=&quot;header-amount&quot;]</value>
      <webElementGuid>07da2009-dd80-40d9-860b-fc2f56769e15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Buy Item/Multiple_promo_applied/iframe_concat(id(, , snap-midtrans, , ))_po_f6bd36</value>
      <webElementGuid>526a58cb-6a31-4ceb-88b2-c84e560ca79e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='header']/div[2]/div/div/div[3]</value>
      <webElementGuid>7a8e73ee-9b0e-40a1-bfd5-03da9a5861f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/following::div[3]</value>
      <webElementGuid>2048ce92-ac15-48e5-977e-c382389429b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Credit/debit card'])[1]/preceding::div[4]</value>
      <webElementGuid>8415598b-5517-47b2-9461-ae9ae8725cd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Rp18.000']/parent::*</value>
      <webElementGuid>54e666b3-017b-4a59-8cc7-18bb2fe2a449</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]</value>
      <webElementGuid>10afe6a3-dec6-435d-91a7-2a2c661093bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Rp18.000' or . = 'Rp18.000')]</value>
      <webElementGuid>1a69c9a8-1895-4b0d-be87-c9386f520606</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
