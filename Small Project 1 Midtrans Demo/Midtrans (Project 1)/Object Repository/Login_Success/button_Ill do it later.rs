<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Ill do it later</name>
   <tag></tag>
   <elementGuidId>8233fb52-bae9-4761-8765-eac61362d621</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.tjr-modal__close.skip-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='onboard-passport-modal']/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3b1616a4-3804-4400-be44-d32255550eb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tjr-modal__close skip-btn</value>
      <webElementGuid>641d1dff-6194-4f1c-bdbd-aca0b468ec0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ga_category</name>
      <type>Main</type>
      <value>Onboarding</value>
      <webElementGuid>3c0aba1c-4a23-4835-8b05-70ff8c6675ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ga_click</name>
      <type>Main</type>
      <value>ob_popup_skip</value>
      <webElementGuid>52fd1992-70ec-42e8-8f23-7c71c337cdc1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
I’ll do it later






</value>
      <webElementGuid>66bda4c2-a8f9-48e3-a0de-1248408bb63e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;onboard-passport-modal&quot;)/div[@class=&quot;tjr-modal&quot;]/button[@class=&quot;tjr-modal__close skip-btn&quot;]</value>
      <webElementGuid>a660ee76-8ba7-4632-9174-ed617858e3a8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='onboard-passport-modal']/div/button</value>
      <webElementGuid>63e96534-f868-4299-8fc7-cb8b83973e7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/following::button[1]</value>
      <webElementGuid>483138e5-d398-472f-8db6-75e0b7d856aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submit'])[1]/following::button[2]</value>
      <webElementGuid>1579478a-ecec-448e-aac3-0e52bf4ecc4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::button[1]</value>
      <webElementGuid>45ded3cc-8ff5-4aab-bd95-27eda6a0349d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/preceding::button[2]</value>
      <webElementGuid>040c279f-2b77-48eb-9210-0226650abc09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='I’ll do it later']/parent::*</value>
      <webElementGuid>dee49663-0c29-4cfc-a509-e1cbf29b6a61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/button</value>
      <webElementGuid>1bb281fc-1c42-4b7e-bc85-7c148e783f64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
I’ll do it later






' or . = '
I’ll do it later






')]</value>
      <webElementGuid>24e43eeb-808e-4524-86ba-f5137903f0f0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
