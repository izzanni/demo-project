<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>message_Invalid email or password</name>
   <tag></tag>
   <elementGuidId>9cb2ca83-c512-4735-97b8-108e8ce67a7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.l-wrong-password.l-wrong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='new_user']/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>36af059c-ea22-4720-9e13-fdaba1151315</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>l-wrong-password l-wrong</value>
      <webElementGuid>6f73a05e-0f3a-417b-ac57-153d98dc9fd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invalid email or password.</value>
      <webElementGuid>9df35389-7c23-4016-9d9f-be52806b537f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;l-password-holder&quot;]/div[@class=&quot;l-wrong-password l-wrong&quot;]</value>
      <webElementGuid>4a083ddb-a5d5-45ac-8803-ef9dc6714450</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div/div[2]</value>
      <webElementGuid>3f82007d-860e-4097-97a6-59c5be0a10b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::div[3]</value>
      <webElementGuid>bfb88275-28a1-4edd-9360-c3a29e63f554</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email Address'])[1]/following::div[3]</value>
      <webElementGuid>18ee3616-2b00-4538-84c3-1aa56a4285df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log me in'])[1]/preceding::div[1]</value>
      <webElementGuid>435f53fb-47c7-46ba-a140-b0f2a6fea14e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Policy'])[1]/preceding::div[1]</value>
      <webElementGuid>9320964c-ad41-4bf3-8c26-a1e1b0210429</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Invalid email or password.']/parent::*</value>
      <webElementGuid>1ef3acb6-45c5-4fa2-b367-86ebdd8c7e3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div[2]</value>
      <webElementGuid>ee4126c8-bf4a-45ca-b018-7faf331102e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Invalid email or password.' or . = 'Invalid email or password.')]</value>
      <webElementGuid>5b2bb2e7-239c-4c13-95be-a4b375d946f5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
