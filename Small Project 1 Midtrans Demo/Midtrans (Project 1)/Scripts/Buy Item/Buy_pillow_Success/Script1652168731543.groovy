import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/button_BUY NOW'))

WebUI.setText(findTestObject('Buy Item/Buy_pillow_Success/input_name'), 'nurul')

WebUI.setText(findTestObject('Buy Item/Buy_pillow_Success/input_email'), 'nurul.qawork@gmail.com')

WebUI.setText(findTestObject('Buy Item/Buy_pillow_Success/input_phone'), '0183719952')

WebUI.setText(findTestObject('Buy Item/Buy_pillow_Success/input_city'), 'Shah Alam')

WebUI.setText(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/input_address'), 'Taman Desa Kemuning')

WebUI.setText(findTestObject('Buy Item/Buy_pillow_Success/input_postalcode'), '40460')

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/button_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/detail_ship_open'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/Customer details'))

WebUI.click(findTestObject('Buy Item/Buy_pillow_Success/detail_ship_close'))

WebUI.click(findTestObject('Buy Item/Buy_pillow_Success/select_method_creditcard'))

WebUI.setText(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/input_Card_number'), '4811111111111114')

WebUI.setText(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/input_Expiration_date'), '0125')

WebUI.setText(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/input_CVV'), '123')

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/select_promo_Potongan 10'))

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/button_Pay now'))

WebUI.setEncryptedText(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/input_Password_TAC'), '4tAN/DuJV7Y=')

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/button_OK'))

WebUI.click(findTestObject('Object Repository/Buy Item/Buy_pillow_Success/button_Back to merchants web'))

