import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

/* Customer_details_forgot_email */
WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/button_BUY NOW'))

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_email'), '.')

WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/button_CHECKOUT'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Input_details_Negative/message_Sorry, something went wrong'), 
    'Sorry, something went wrong.')

/* Customer_details_not_complete */
WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/button_BUY NOW'))

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_email'), 'nurul.qawork@gmail.com')

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_city'), '.')

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_address'), 
    '.')

WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/button_CHECKOUT'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Input_details_Negative/title_COCO STORE'), 'COCO STORE')

/* Card_details_wrong_number */
WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/select_method_creditcard'))

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_Card_number'), '4911111111111113')

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_Expiration_date'), '0125')

WebUI.setText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_CVV_card-cvv'), '123')

WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/button_Pay now'))

WebUI.setEncryptedText(findTestObject('Object Repository/Buy Item/Input_details_Negative/input_Password_TAC'), '4tAN/DuJV7Y=')

WebUI.click(findTestObject('Object Repository/Buy Item/Input_details_Negative/button_OK'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Input_details_Negative/message_Card got declined by the bank. Try usin_28c355'), 
    'Card got declined by the bank. Try using another card/payment method.')

WebUI.closeBrowser()

