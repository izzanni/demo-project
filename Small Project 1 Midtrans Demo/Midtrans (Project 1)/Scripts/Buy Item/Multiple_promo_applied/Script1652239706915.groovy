import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/button_BUY NOW'))

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/button_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/select_method_creditcard'))

WebUI.setText(findTestObject('Buy Item/Multiple_promo_applied/input_Card_number'), '4811111111111114')

WebUI.setText(findTestObject('Buy Item/Multiple_promo_applied/input_Expiration_date'), '0125')

WebUI.setText(findTestObject('Buy Item/Multiple_promo_applied/input_CVV'), '123')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/promo_Proceed without promo'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/div_Rp20.000'), 'Rp20.000')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/promo_Potongan 10 Rupiah'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/div_Rp19.990'), 'Rp19.990')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/promo_Promo Midtrans'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/div_Rp18.000'), 'Rp18.000')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/promo_Potongan 10 - Demo Promo Engine'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/div_Rp18.000'), 'Rp18.000')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/promo_Promo Installment'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/div_Rp15.000'), 'Rp15.000')

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/button_back'))

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/button_Yes, cancel'))

WebUI.click(findTestObject('Object Repository/Buy Item/Multiple_promo_applied/button_close_COCO STORE_logo'))

