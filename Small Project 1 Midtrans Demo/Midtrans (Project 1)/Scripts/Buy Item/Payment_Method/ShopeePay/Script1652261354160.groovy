import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/ShopeePay/a_BUY NOW'))

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/ShopeePay/div_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/ShopeePay/div_ShopeePayother e-Wallets_list-payment-logo'))

WebUI.click(findTestObject('Buy Item/Payment_Method/ShopeePay/button_I have paid'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Payment_Method/ShopeePay/div_Your transaction is being processed'), 
    'Your transaction is being processed')

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/ShopeePay/button_Back to merchants web'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Payment_Method/ShopeePay/span_Weve booked your payment'), 
    'We\'ve booked your payment.')

WebUI.closeBrowser()

