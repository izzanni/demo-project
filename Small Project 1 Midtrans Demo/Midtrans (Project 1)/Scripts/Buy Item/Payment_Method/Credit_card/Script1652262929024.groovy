import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/a_BUY NOW'))

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/div_CHECKOUT'))

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/div_Promo available_list-payment-logo'))

WebUI.setText(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/input_Card number_valid-input-value'), 
    '4811111111111114')

WebUI.setText(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/input_Expiration date_card-expiry'), 
    '0125')

WebUI.setText(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/input_CVV_card-cvv'), '123')

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/button_Pay now'))

WebUI.setEncryptedText(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/input_Password_PaRes'), 
    '4tAN/DuJV7Y=')

WebUI.click(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/button_OK'))

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/div_Payment successful'), 
    'Payment successful')

WebUI.verifyElementText(findTestObject('Object Repository/Buy Item/Payment_Method/Creditcard/span_Thank you for your purchase'), 
    'Thank you for your purchase.')

