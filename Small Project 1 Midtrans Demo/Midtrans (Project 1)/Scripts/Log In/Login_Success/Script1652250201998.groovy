import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dashboard.midtrans.com/login')

def email = 'nurul.qawork@gmail.com'

def password = 'Sarang3%'

def invalidpassword = 'Sara123456'

/*
 * Negative Case
 */
WebUI.setText(findTestObject('Login_Success/input_Email'), email)

WebUI.setText(findTestObject('Login_Success/input_Password'), invalidpassword)

WebUI.click(findTestObject('Login_Success/button_Log me in'))

WebUI.verifyElementText(findTestObject('Login_Success/message_Invalid email or password'), 'Invalid email or password.')

/*
 * Positive Case
 */
WebUI.click(findTestObject('Login_Success/button_Log me in'))

WebUI.setText(findTestObject('Object Repository/Login_Success/input_Email'), email)

WebUI.setText(findTestObject('Object Repository/Login_Success/input_Password'), password)

WebUI.click(findTestObject('Object Repository/Login_Success/button_Log me in'))

WebUI.click(findTestObject('Object Repository/Login_Success/button_Ill do it later'))

WebUI.verifyElementText(findTestObject('Object Repository/Login_Success/display_username'), 'sekolahqa')

WebUI.verifyElementText(findTestObject('Object Repository/Login_Success/display_email'), 'nurul.qawork@gmail.com')

WebUI.closeBrowser()